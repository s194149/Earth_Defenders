#include "ansi.h"
#include "LUT.h"



void clrscr() {

  uint8_t type = 2;
  printf("%c[%d;J", ESC, type);

}

void clreol() {

  printf("%c[K", ESC);

}

void underline(uint8_t on) {

  uint8_t a;
  if (on == 1) {

    a = 4;

  }
  else {

    a = 24;

  }
  printf("%c[%dm", ESC, a);

}

void blink(uint8_t on) {

  uint8_t a;
  if (on == 1) {

    a = 5;

    printf("%c[%dm", ESC, a);

  }
}

void inverse(uint8_t on) {

  uint8_t a;
  if (on == 1) {

    a = 7;

    printf("%c[%dm", ESC, a);

  }
}

void gotoxy(uint8_t x, uint8_t y) {

  printf("%c[%d;%dH", ESC, y, x);

}


void normal(uint8_t on) {

  uint8_t a;
  if (on == 1) {

    a = 0;

    printf("%c[%dm", ESC, a);

  }
}

void window1(uint8_t x_1,uint8_t y_1, uint8_t x_2, uint8_t y_2, char name[], uint8_t style) {

  clrscr();

  if (style == 1) {
    color(5,0);
  }
  else if (style == 2) {
    color(1,0);
  }
  else color(15,0);

  gotoxy(x_1,y_1);
  int32_t a= x_2 - x_1;
  int32_t b= y_2 - y_1;
  int32_t i;

  for (i = 1; i <= a; i++) {
    printf("_");
  }

  gotoxy(x_1,y_2);

  for (i = 1; i <= a; i++) {
    printf("_");
  }

  gotoxy(x_1,y_1);
  for (i = 1; i <= b; i++) {
    printf("%c[%dB", ESC, 1);
    printf("%c[%dD", ESC, 1);
    printf("%c",179);
  }

  gotoxy(x_2,y_1);
  for (i = 1; i <= b; i++) {
    printf("%c[%dB", ESC, 1);
    printf("%c[%dD", ESC, 1);
    printf("%c",179);
  }

  gotoxy(a / 2 + x_1 - (strlen(name)/2),y_1 +1);
  printf("%s",name);
}

void window2(uint8_t x_1,uint8_t y_1, uint8_t x_2, uint8_t y_2, char name[], uint8_t style) {

  if (style == 1) {
    color(5,0);
  }
  else if (style == 2) {
    color(1,0);
  }
  else color(15,0);

  gotoxy(x_1,y_1);
  int32_t a= x_2 - x_1;
  int32_t b= y_2 - y_1;
  int32_t i;

  for (i = 1; i <= a; i++) {
    printf("%c",205);
  }

  gotoxy(x_1,y_2);
  for (i = 1; i <= a; i++) {
    printf("%c",205);
  }

  gotoxy(x_1,y_1);
  for (i = 1; i <= b; i++) {
    printf("%c[%dB", ESC, 1);
    printf("%c[%dD", ESC, 1);
    printf("%c",186);
  }

  gotoxy(x_2,y_1);
  for (i = 1; i <= b; i++) {
    printf("%c[%dB", ESC, 1);
    printf("%c[%dD", ESC, 1);
    printf("%c",186);
  }

  gotoxy(a / 2 + x_1 - (strlen(name)/2) -1,y_1 +1);
  printf("%s",name);
  gotoxy(x_1 -1,y_1);
  printf("%c",201);
  gotoxy(x_2-1,y_1);
  printf("%c",187);
  gotoxy(x_1-1,y_2);
  printf("%c",200);
  gotoxy(x_2-1,y_2);
  printf("%c",188);
}




int32_t trigonometri1(int32_t d) {

  int32_t c;
  c=(d & 511);

  return SIN[c];
}

int32_t trigonometri2(int32_t d) {

  int32_t c;
  c=((d+128) & 511);

  return SIN[c];
}

int32_t angler(int32_t v) {

  return v*512/360;
}

int32_t rotateVector(int32_t x, int32_t y,int32_t d) {

  int32_t X,Y,a,b,c,e;
  a=trigonometri2(angler(d));
  c=trigonometri1(angler(d));
  b = a << 2;
  e = c << 2;

  X=x*b-(y*e);
  Y=x*e+(y*b);

  printFix(X);
  printFix(Y);
}




void omake(struct ball *o) {

  gotoxy(o->x,o->y);
  printf("%c",111);
}

void omdel(struct ball *o) {

  gotoxy(o->x +1,o->y);
  printf("%c",127);
}

void oupdate(struct ball *o){

  o->x += o->vx;
  o->y += o->vy;
}

void hits(struct ball *o) {

  gotoxy(38,23);
  printf("%d",o->hit);
}

void cursortype(int32_t type) {

  if (type == 1) {
    printf("\e[?25l");
  }
  else printf("\e[?25h");
}

void wallcheck(struct ball *o) {

  if (o->x == 2) {
    o->vx*=-1;
    o->hit+=1;
  }

  if (o->x == 78) {
    o->vx*=-1;
    o->hit+=1;
  }

  if (o->y == 2) {
    o->vy*=-1;
    o->hit+=1;
  }

  if (o->y == 19) {
    o->vy*=-1;
    o->hit+=1;
  }
}




char readJoystick() {

  char z=0;

  RCC->AHBENR |= RCC_AHBPeriph_GPIOC; // Enable clock for GPIO Port A
  // Set pin PA0 to input
  GPIOC->MODER &= ~(0x00000003 << (0 * 2)); // Clear mode register
  GPIOC->MODER |= (0x00000000 << (0 * 2)); // Set mode register (0x00 �
  //Input, 0x01 - Output, 0x02 - Alternate Function, 0x03 - Analog in/out)
  GPIOC->PUPDR &= ~(0x00000003 << (0 * 2)); // Clear push/pull register
  GPIOC->PUPDR |= (0x00000002 << (0 * 2)); // Set push/pull register (0x00 -No pull, 0x01 - Pull-up, 0x02 - Pull-down)

  uint16_t valR = GPIOC->IDR & (0x0001 << 0); //Read from pin PA0

  RCC->AHBENR |= RCC_AHBPeriph_GPIOA; // Enable clock for GPIO Port A
  // Set pin PA0 to input
  GPIOA->MODER &= ~(0x00000003 << (4 * 2)); // Clear mode register
  GPIOA->MODER |= (0x00000000 << (4 * 2)); // Set mode register (0x00 �
  //Input, 0x01 - Output, 0x02 - Alternate Function, 0x03 - Analog in/out)
  GPIOA->PUPDR &= ~(0x00000003 << (4 * 2)); // Clear push/pull register
  GPIOA->PUPDR |= (0x00000002 << (4 * 2)); // Set push/pull register (0x00 -No pull, 0x01 - Pull-up, 0x02 - Pull-down)

  uint16_t valU = GPIOA->IDR & (0x0001 << 4); //Read from pin PA0
  valU/=16;

  RCC->AHBENR |= RCC_AHBPeriph_GPIOB; // Enable clock for GPIO Port A
  // Set pin PA0 to input
  GPIOB->MODER &= ~(0x00000003 << (5 * 2)); // Clear mode register
  GPIOB->MODER |= (0x00000000 << (5 * 2)); // Set mode register (0x00 �
  //Input, 0x01 - Output, 0x02 - Alternate Function, 0x03 - Analog in/out)
  GPIOB->PUPDR &= ~(0x00000003 << (5 * 2)); // Clear push/pull register
  GPIOB->PUPDR |= (0x00000002 << (5 * 2)); // Set push/pull register (0x00 -No pull, 0x01 - Pull-up, 0x02 - Pull-down)

  uint16_t valC = GPIOB->IDR & (0x0001 << 5); //Read from pin PA0
  valC/=32;

  RCC->AHBENR |= RCC_AHBPeriph_GPIOC; // Enable clock for GPIO Port A
  // Set pin PA0 to input
  GPIOC->MODER &= ~(0x00000003 << (1 * 2)); // Clear mode register
  GPIOC->MODER |= (0x00000000 << (1 * 2)); // Set mode register (0x00 �
  //Input, 0x01 - Output, 0x02 - Alternate Function, 0x03 - Analog in/out)
  GPIOC->PUPDR &= ~(0x00000003 << (1 * 2)); // Clear push/pull register
  GPIOC->PUPDR |= (0x00000002 << (1 * 2)); // Set push/pull register (0x00 -No pull, 0x01 - Pull-up, 0x02 - Pull-down)

  uint16_t valL = GPIOC->IDR & (0x0001 << 1); //Read from pin PA0
  valL/=2;

  RCC->AHBENR |= RCC_AHBPeriph_GPIOB; // Enable clock for GPIO Port A
  // Set pin PA0 to input
  GPIOB->MODER &= ~(0x00000003 << (0 * 2)); // Clear mode register
  GPIOB->MODER |= (0x00000000 << (0 * 2)); // Set mode register (0x00 �
  //Input, 0x01 - Output, 0x02 - Alternate Function, 0x03 - Analog in/out)
  GPIOB->PUPDR &= ~(0x00000003 << (0 * 2)); // Clear push/pull register
  GPIOB->PUPDR |= (0x00000002 << (0 * 2)); // Set push/pull register (0x00 -No pull, 0x01 - Pull-up, 0x02 - Pull-down)

  uint16_t valD = GPIOB->IDR & (0x0001 << 0); //Read from pin PA0

  z|= valU << 0;
  z|= valD << 1;
  z|= valL << 2;
  z|= valR << 3;
  z|= valC << 4;
  z|= 0 << 5;
  z|= 0 << 6;
  z|= 0 << 7;

  return z;
}

void pasteJoystick (char j) {

  if (j == 1) {
    printf("up\n");
  }
  if (j == 2) {
    printf("down\n");
  }
  if (j == 4) {
   printf("left\n");
  }
  if (j == 8) {
    printf("right\n");
  }
  if (j == 16) {
    printf("center\n");
  }
  if (j == 17) {
    printf("up and center\n");
  }
  if (j == 18) {
    printf("down and center\n");
  }
  if (j == 20) {
    printf("left and center\n");
  }
  if (j == 24) {
    printf("right and center\n");
  }
}

void setLed(char j) {

  RCC->AHBENR |= RCC_AHBPeriph_GPIOB;
  RCC->AHBENR |= RCC_AHBPeriph_GPIOA;
  RCC->AHBENR |= RCC_AHBPeriph_GPIOC;

  GPIOB->MODER &= ~(0x00000003 << (4 * 2)); // Clear mode register
  GPIOB->MODER |= (0x00000000 << (4 * 2)); // Set mode register (0x00 �Input, 0x01 - Output, 0x02 - Alternate Function, 0x03 - Analog in/out

  GPIOC->MODER &= ~(0x00000003 << (7 * 2)); // Clear mode register
  GPIOC->MODER |= (0x00000000 << (7 * 2)); // Set mode register (0x00 �Input, 0x01 - Output, 0x02 - Alternate Function, 0x03 - Analog in/out)

  GPIOA->MODER &= ~(0x00000003 << (9 * 2)); // Clear mode register
  GPIOA->MODER |= (0x00000000 << (9 * 2)); // Set mode register (0x00 �Input, 0x01 - Output, 0x02 - Alternate Function, 0x03 - Analog in/out)

  if (j == 1) {
    // Set pin PA1 to output
    GPIOB->OSPEEDR &= ~(0x00000003 << (4 * 2)); // Clear speed register
    GPIOB->OSPEEDR |= (0x00000002 << (4 * 2)); // set speed register (0x01 - 10 MHz, 0x02 - 2 MHz, 0x03 - 50 MHz)
    GPIOB->OTYPER &= ~(0x0001 << (4)); // Clear output type register
    GPIOB->OTYPER |= (0x0000 << (4)); // Set output type register (0x00 -Push pull, 0x01 - Open drain)
    GPIOB->MODER &= ~(0x00000003 << (4 * 2)); // Clear mode register
    GPIOB->MODER |= (0x00000001 << (4 * 2)); // Set mode register (0x00 �Input, 0x01 - Output, 0x02 - Alternate Function, 0x03 - Analog in/out
    GPIOB->ODR |= (0x0001 << 1); //Set pin PA1 to high
    }

  if (j == 2) {
    // Set pin PA1 to output
    GPIOC->OSPEEDR &= ~(0x00000003 << (7 * 2)); // Clear speed register
    GPIOC->OSPEEDR |= (0x00000002 << (7 * 2)); // set speed register (0x01 - 10 MHz, 0x02 - 2 MHz, 0x03 - 50 MHz)
    GPIOC->OTYPER &= ~(0x0001 << (7)); // Clear output type register
    GPIOC->OTYPER |= (0x0000 << (7)); // Set output type register (0x00 -Push pull, 0x01 - Open drain)
    GPIOC->MODER &= ~(0x00000003 << (7 * 2)); // Clear mode register
    GPIOC->MODER |= (0x00000001 << (7 * 2)); // Set mode register (0x00 �Input, 0x01 - Output, 0x02 - Alternate Function, 0x03 - Analog in/out)
    GPIOC->ODR |= (0x0001 << 1); //Set pin PA1 to high
    }

  if (j == 4) {
    // Set pin PA1 to output
    GPIOA->OSPEEDR &= ~(0x00000003 << (9 * 2)); // Clear speed register
    GPIOA->OSPEEDR |= (0x00000002 << (9 * 2)); // set speed register (0x01 - 10 MHz, 0x02 - 2 MHz, 0x03 - 50 MHz)
    GPIOA->OTYPER &= ~(0x0001 << (9)); // Clear output type register
    GPIOA->OTYPER |= (0x0000 << (9)); // Set output type register (0x00 -Push pull, 0x01 - Open drain)
    GPIOA->MODER &= ~(0x00000003 << (9 * 2)); // Clear mode register
    GPIOA->MODER |= (0x00000001 << (9 * 2)); // Set mode register (0x00 �Input, 0x01 - Output, 0x02 - Alternate Function, 0x03 - Analog in/out)
    GPIOA->ODR |= (0x0001 << 1); //Set pin PA1 to high
  }
  if (j == 8) {
    // Set pin PA1 to output
    GPIOA->OSPEEDR &= ~(0x00000003 << (9 * 2)); // Clear speed register
    GPIOA->OSPEEDR |= (0x00000002 << (9 * 2)); // set speed register (0x01 - 10 MHz, 0x02 - 2 MHz, 0x03 - 50 MHz)
    GPIOA->OTYPER &= ~(0x0001 << (9)); // Clear output type register
    GPIOA->OTYPER |= (0x0000 << (9)); // Set output type register (0x00 -Push pull, 0x01 - Open drain)
    GPIOA->MODER &= ~(0x00000003 << (9 * 2)); // Clear mode register
    GPIOA->MODER |= (0x00000001 << (9 * 2)); // Set mode register (0x00 �Input, 0x01 - Output, 0x02 - Alternate Function, 0x03 - Analog in/out)
    GPIOA->ODR |= (0x0001 << 1); //Set pin PA1 to high

    // Set pin PA1 to output
    GPIOC->OSPEEDR &= ~(0x00000003 << (7 * 2)); // Clear speed register
    GPIOC->OSPEEDR |= (0x00000002 << (7 * 2)); // set speed register (0x01 - 10 MHz, 0x02 - 2 MHz, 0x03 - 50 MHz)
    GPIOC->OTYPER &= ~(0x0001 << (7)); // Clear output type register
    GPIOC->OTYPER |= (0x0000 << (7)); // Set output type register (0x00 -Push pull, 0x01 - Open drain)
    GPIOC->MODER &= ~(0x00000003 << (7 * 2)); // Clear mode register
    GPIOC->MODER |= (0x00000001 << (7 * 2)); // Set mode register (0x00 �Input, 0x01 - Output, 0x02 - Alternate Function, 0x03 - Analog in/out)
    GPIOC->ODR |= (0x0001 << 1); //Set pin PA1 to high
  }

  if (j == 16) {
    // Set pin PA1 to output
    GPIOC->OSPEEDR &= ~(0x00000003 << (7 * 2)); // Clear speed register
    GPIOC->OSPEEDR |= (0x00000002 << (7 * 2)); // set speed register (0x01 - 10 MHz, 0x02 - 2 MHz, 0x03 - 50 MHz)
    GPIOC->OTYPER &= ~(0x0001 << (7)); // Clear output type register
    GPIOC->OTYPER |= (0x0000 << (7)); // Set output type register (0x00 -Push pull, 0x01 - Open drain)
    GPIOC->MODER &= ~(0x00000003 << (7 * 2)); // Clear mode register
    GPIOC->MODER |= (0x00000001 << (7 * 2)); // Set mode register (0x00 �Input, 0x01 - Output, 0x02 - Alternate Function, 0x03 - Analog in/out)
    GPIOC->ODR |= (0x0001 << 1); //Set pin PA1 to high

    // Set pin PA1 to output
    GPIOB->OSPEEDR &= ~(0x00000003 << (4 * 2)); // Clear speed register
    GPIOB->OSPEEDR |= (0x00000002 << (4 * 2)); // set speed register (0x01 - 10 MHz, 0x02 - 2 MHz, 0x03 - 50 MHz)
    GPIOB->OTYPER &= ~(0x0001 << (4)); // Clear output type register
    GPIOB->OTYPER |= (0x0000 << (4)); // Set output type register (0x00 -Push pull, 0x01 - Open drain)
    GPIOB->MODER &= ~(0x00000003 << (4 * 2)); // Clear mode register
    GPIOB->MODER |= (0x00000001 << (4 * 2)); // Set mode register (0x00 �Input, 0x01 - Output, 0x02 - Alternate Function, 0x03 - Analog in/out
    GPIOB->ODR |= (0x0001 << 1); //Set pin PA1 to high
  }
}






void inputtermdir(){

clrscr();

uart_clear();

cursortype(1);

gotoxy(2,2);

int8_t j=2;


while(1){

j=uart_get_char();

if (j != 0){

printf("%c",j);


}
}


}

void inputtermrest(){

clrscr();

gotoxy(1,1);

uart_clear();

cursortype(1);

int8_t j;

int8_t n=0;

uint8_t arr[11];



while(1){

j=uart_get_char();

if (j != 0){


arr[n]=j;

printf("%c",j);

n++;

}

if (j == 0x0D) {

    clrscr();
    gotoxy(1,1);

    printf("%s\n",arr);

    n=0;


}

if (n == 10) {

    clrscr();
    gotoxy(1,1);

    printf("%s\n",arr);

    n=0;


}
}


}




void lcd_update(){




}

void clrme(){

    uint8_t arr[512];
    lcd_init();
    memset(arr,0x00,512);
    lcd_push_buffer(arr);
}

void lcd_write_string(char a[],int loc){




const char ch[95][5] = {
  {0x00, 0x00, 0x00, 0x00, 0x00},
  {0x00, 0x5F, 0x5F, 0x00, 0x00},
  {0x00, 0x07, 0x00, 0x07, 0x00},
  {0x14, 0x7F, 0x14, 0x7F, 0x14},
  {0x24, 0x2A, 0x7F, 0x2A, 0x12},
  {0x23, 0x13, 0x08, 0x64, 0x62},
  {0x36, 0x49, 0x55, 0x22, 0x50},
  {0x00, 0x05, 0x03, 0x00, 0x00},
  {0x00, 0x1C, 0x22, 0x41, 0x00},
  {0x00, 0x41, 0x22, 0x1C, 0x00},
  {0x14, 0x08, 0x3E, 0x08, 0x14},
  {0x08, 0x08, 0x3E, 0x08, 0x08},
  {0x00, 0x50, 0x30, 0x00, 0x00},
  {0x08, 0x08, 0x08, 0x08, 0x08},
  {0x00, 0x60, 0x60, 0x00, 0x00},
  {0x20, 0x10, 0x08, 0x04, 0x02},
  {0x3E, 0x51, 0x49, 0x45, 0x3E},
  {0x00, 0x42, 0x7F, 0x40, 0x00},
  {0x42, 0x61, 0x51, 0x49, 0x46},
  {0x22, 0x49, 0x49, 0x49, 0x36},
  {0x18, 0x14, 0x12, 0x7F, 0x10},
  {0x2F, 0x49, 0x49, 0x49, 0x31},
  {0x3E, 0x49, 0x49, 0x49, 0x32},
  {0x03, 0x01, 0x71, 0x09, 0x07},
  {0x36, 0x49, 0x49, 0x49, 0x36},
  {0x26, 0x49, 0x49, 0x49, 0x3E},
  {0x00, 0x36, 0x36, 0x00, 0x00},
  {0x00, 0x56, 0x36, 0x00, 0x00},
  {0x08, 0x14, 0x22, 0x41, 0x00},
  {0x14, 0x14, 0x14, 0x14, 0x14},
  {0x00, 0x41, 0x22, 0x14, 0x08},
  {0x02, 0x01, 0x51, 0x09, 0x06},
  {0x32, 0x49, 0x79, 0x41, 0x3E},
  {0x7C, 0x0A, 0x09, 0x0A, 0x7C},
  {0x7F, 0x49, 0x49, 0x49, 0x36},
  {0x3E, 0x41, 0x41, 0x41, 0x22},
  {0x7F, 0x41, 0x41, 0x41, 0x3E},
  {0x7F, 0x49, 0x49, 0x49, 0x41},
  {0x7F, 0x09, 0x09, 0x09, 0x01},
  {0x3E, 0x41, 0x49, 0x49, 0x7A},
  {0x7F, 0x08, 0x08, 0x08, 0x7F},
  {0x00, 0x41, 0x7F, 0x41, 0x00},
  {0x30, 0x40, 0x40, 0x40, 0x3F},
  {0x7F, 0x08, 0x14, 0x22, 0x41},
  {0x7F, 0x40, 0x40, 0x40, 0x40},
  {0x7F, 0x02, 0x0C, 0x02, 0x7F},
  {0x7F, 0x02, 0x04, 0x08, 0x7F},
  {0x3E, 0x41, 0x41, 0x41, 0x3E},
  {0x7F, 0x09, 0x09, 0x09, 0x06},
  {0x3E, 0x41, 0x51, 0x21, 0x5E},
  {0x7F, 0x09, 0x09, 0x09, 0x76},
  {0x26, 0x49, 0x49, 0x49, 0x32},
  {0x01, 0x01, 0x7F, 0x01, 0x01},
  {0x3F, 0x40, 0x40, 0x40, 0x3F},
  {0x1F, 0x20, 0x40, 0x20, 0x1F},
  {0x3F, 0x40, 0x38, 0x40, 0x3F},
  {0x63, 0x14, 0x08, 0x14, 0x63},
  {0x03, 0x04, 0x78, 0x04, 0x03},
  {0x61, 0x51, 0x49, 0x45, 0x43},
  {0x7F, 0x41, 0x41, 0x00, 0x00},
  {0x02, 0x04, 0x08, 0x10, 0x20},
  {0x00, 0x41, 0x41, 0x7F, 0x00},
  {0x04, 0x02, 0x01, 0x02, 0x04},
  {0x40, 0x40, 0x40, 0x40, 0x40},
  {0x00, 0x01, 0x02, 0x04, 0x00},
  {0x20, 0x54, 0x54, 0x54, 0x78},
  {0x7F, 0x48, 0x44, 0x44, 0x38},
  {0x38, 0x44, 0x44, 0x44, 0x20},
  {0x38, 0x44, 0x44, 0x48, 0x7F},
  {0x38, 0x54, 0x54, 0x54, 0x18},
  {0x08, 0x7E, 0x09, 0x01, 0x02},
  {0x0C, 0x52, 0x52, 0x52, 0x3E},
  {0x7F, 0x08, 0x04, 0x04, 0x78},
  {0x00, 0x44, 0x7D, 0x40, 0x00},
  {0x20, 0x40, 0x44, 0x3D, 0x00},
  {0x7F, 0x10, 0x28, 0x44, 0x00},
  {0x00, 0x41, 0x7F, 0x40, 0x00},
  {0x7C, 0x04, 0x18, 0x04, 0x78},
  {0x7C, 0x08, 0x04, 0x04, 0x78},
  {0x38, 0x44, 0x44, 0x44, 0x38},
  {0x7C, 0x14, 0x14, 0x14, 0x08},
  {0x08, 0x14, 0x14, 0x18, 0x7C},
  {0x7C, 0x08, 0x04, 0x04, 0x08},
  {0x48, 0x54, 0x54, 0x54, 0x20},
  {0x04, 0x3F, 0x44, 0x40, 0x20},
  {0x3C, 0x40, 0x40, 0x20, 0x7C},
  {0x1C, 0x20, 0x40, 0x20, 0x1C},
  {0x3C, 0x40, 0x38, 0x40, 0x3C},
  {0x44, 0x28, 0x10, 0x28, 0x44},
  {0x0C, 0x50, 0x50, 0x50, 0x3C},
  {0x44, 0x64, 0x54, 0x4C, 0x44},
  {0x00, 0x08, 0x36, 0x41, 0x00},
  {0x00, 0x00, 0x7F, 0x00, 0x00},
  {0x00, 0x41, 0x36, 0x08, 0x00},
  {0x08, 0x04, 0x08, 0x10, 0x08}};


uint8_t arr[512];

int j;

for (j=0;j <= 512; j++) {

arr[j]=0x00;

}


uint8_t i;

for (i=0; i <= strlen(a)-1; i++){

arr[loc +0 +(i*7)]=ch[a[i]-32][0];
arr[loc +1 +(i*7)]=ch[a[i]-32][1];
arr[loc +2 +(i*7)]=ch[a[i]-32][2];
arr[loc +3 +(i*7)]=ch[a[i]-32][3];
arr[loc +4 +(i*7)]=ch[a[i]-32][4];


}



lcd_push_buffer(arr);

}

void init_L() {

    L.a = 0;
    L.b = 0;
    L.c = 0;
    L.c = 0;

}

void Timelcd (){


clrscr();

init_L();

cursortype(1);

init_timer2();



uart_clear();





int8_t j;

int8_t n=0;

uint8_t arr[100];

int8_t k;



while(1){



j=uart_get_char();

if (j != 0){


arr[n]=j;

printf("%c",j);

n++;
}


if (L.a == 5) {

    clrscr();
    gotoxy(1,1);
    lcd_write_string(arr,1);


    for (k=1;k < 100; k++) {

    arr[0]=32;
    arr[k]=0;

    }

    L.a=0;

    n=0;

}

}
}









