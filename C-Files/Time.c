#include "ansi.h"
#include "LUT.h"


void init_timer2() {

  RCC->APB1ENR |= RCC_APB1Periph_TIM2; // Enable clock line to timer 2;
  TIM2->ARR = 6138; // Set reload value
  TIM2->PSC = 100; // Set prescale value
  TIM2->CR1=0;

  TIM2->CR1 |= (0x0000 << (11)); // UI remapping
  TIM2->CR1 |= (0x0000 << (8)); // ckd
  TIM2->CR1 |= (0x0000 << (9)); // ckd
  TIM2->CR1 |= (0x0000 << (7)); // arpe
  TIM2->CR1 |= (0x0000 << (5)); // cms
  TIM2->CR1 |= (0x0000 << (6)); // cms
  TIM2->CR1 |= (0x0000 << (4));// dir
  TIM2->CR1 |= (0x0000 << (3));// opm
  TIM2->CR1 |= (0x0000 << (2));//URS
  TIM2->CR1 |= (0x0000 << (1));//UDIS
  TIM2->CR1 |= (0x0001 << (0));//start
  TIM2->DIER |= 0x0001;
  NVIC_SetPriority(TIM2_IRQn, 1); // Set interrupt priority
  NVIC_EnableIRQ(TIM2_IRQn); // Enable interrupt

}

void TIM2_IRQHandler(void) {
 //Do whatever you want here, but make sure it doesn�t take too much time!

        T_player.hs+=1;
        T_bullet.hs+=1;
        T_enemy.hs+=1;
        T_enemybullet.hs+=1;
        T_score.hs+=1;
        T_comet.hs+=1;
        T_time.hs+=1;


        if (T_player.hs == 2){
            T_player.hs=0;
            T_player.flag = 1;

        }
        if (T_bullet.hs == 2){
            T_bullet.hs=0;
            T_bullet.flag = 1;

        }
        if (T_score.hs == 50){
            T_score.hs=0;
            T_score.flag = 1;

        }
        if (T_time.hs == 100){
            T_time.hs=0;
            T_time.flag += 1;

        }



        if (T_level.s == 2){
            T_level.s=0;
            T_level.flag = 0;

        }



         if (T_comet.s >=5){

         if (T_comet.hs == 20){
            T_comet.hs=0;
            T_comet.flag = 1;

        }
        }

        if (T_comet.s == 20){
            T_comet.s=0;
        }

        if (T_enemy.hs == 4){
            T_enemy.hs=0;
            T_enemy.flag = 1;

        }


        if (T_enemybullet.hs == 1){
            T_enemybullet.hs=0;
            T_enemybullet.flag = 1;

        }




        T.hs+=1;
        T1.hs+=1;

        if (T.hs >= 99){
            T.s+=1;
            T.hs=0;
            T_comet.s+=1;
            T_powerup.s+=1;
            if (T_level.flag == 1){
                T_level.s+=1;
            }
        }
        if (T.s >= 59){
            T.m+=1;
            T.s=0;

        }
        if (T.m >= 59){
            T.h+=1;
            T.m=0;
        }


 TIM2->SR &= ~0x0001; // Clear interrupt bit
 }

void init_timepoint2() {

    T.h = 0;
    T.m = 0;
    T.s = 0;
    T.hs = 0;

}

void init_timepoint() {

    T1.h = 0;
    T1.m = 0;
    T1.s = 0;
    T1.hs = 0;

    T_time.flag=0;
    T_time.hs=0;

}

void timesplit(char j) {

    int l=0;

  if (j == 2) {
    init_timepoint2();
  }
  if (j == 4) {
    gotoxy(2,4);
    printf("Split time 1: %d,%d,%d,%d\n",T.h,T.m,T.s,T.hs);
  }
  if (j == 8) {
    gotoxy(2,5);
    printf("Split time 2: %d,%d,%d,%d\n",T.h,T.m,T.s,T.hs);
  }
  if (j == 1) {

    if (TIM2->CR1 == 0){

        TIM2->CR1 |= (0x0001 << (0));//start

    }
    else {

        TIM2->CR1=0;

    }
    }
}

void rnd(){

uint8_t c;
uint8_t n;

 srand(time(2));
for (c = 1; c <= 10; c++) {
    n = rand() % 20 + 1;
  }





}



