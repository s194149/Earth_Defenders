#ifndef _ansi_H_
#define _ansi_H_

/* Includes ------------------------------------------------------------------*/
#include "stm32f30x_conf.h"
#include <stdio.h>
#include <stdint.h>

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
#define ESC 0x1B

/* Exported functions ------------------------------------------------------- */
void fgcolor(uint8_t foreground);
void bgcolor(uint8_t background);
void color(uint8_t foreground, uint8_t background);
void clrscr();
void clreol();

struct TVector {
	int32_t x;
	int32_t y;
};

struct ball {
	int32_t x;
	int32_t y;
	int32_t vx;
	int32_t vy;
	int32_t hit;
};

struct joy {
	int32_t u;
	int32_t d;
	int32_t l;
	int32_t r;
	int32_t c;
	int32_t rest1;
	int32_t rest2;
	int32_t rest3;
};

struct joy2 {
	int32_t u;
	int32_t d;
	int32_t l;
	int32_t r;
	int32_t c;
	int32_t rest1;
	int32_t rest2;
	int32_t rest3;
};


struct Timer {

   volatile uint8_t h;
   volatile uint8_t m;
   volatile uint8_t s;
   volatile uint8_t hs;

};

struct Timer T;


struct LCD {

   volatile uint8_t a;
   volatile uint8_t b;
   volatile uint8_t c;
   volatile uint8_t d;

};

struct LCD L;






#endif /* _30010_IO_H_ */
